<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $data['title']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL;?>/css/bootstrap.css">

</head>
<body>
    
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="<?= BASEURL;?>"><strong>PHP MVC</strong></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="<?= BASEURL;?>">Home</a>
        <a class="nav-link" href="<?= BASEURL;?>/User">User</a>
        <a class="nav-link" href="<?= BASEURL;?>/blog">Blog</a>
        <a class="nav-link" href="<?= BASEURL;?>/User/login">Masuk</a>
      </div>
    </div>
</nav>

</div>